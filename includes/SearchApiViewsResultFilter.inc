<?php

/**
 * Class SearchApiViewsResultFilter.
 */
class SearchApiViewsResultFilter extends SearchApiAbstractAlterCallback {

  /**
   * The view.
   *
   * @var view
   */
  private $view;

  /**
   * The configuration form to select the view and display.
   *
   * @return array
   *   The form element array.
   */
  public function configurationForm() {
    $form['view'] = [
      '#type' => 'select',
      '#title' => t('View mode'),
      '#options' => $this->getViewsOptionList(),
      '#description' => t('The first parameter will be the id of the entity (nid), Use with caution: Heavy views can affect performance.'),
      '#default_value' => isset($this->options['view']) ? $this->options['view'] : '',
    ];

    return $form;
  }

  /**
   * Gets the options of views that are selectable.
   *
   * @return array
   *   Array of views.
   */
  private function getViewsOptionList() {
    $views = views_get_all_views();

    $option_list = [];

    /** @var view $view */
    foreach ($views as $view) {
      /** @var \views_display $view_display */
      foreach ($view->display as $system_name => $view_display) {
        $option_list[$view->human_name][$view->name . '::' . $system_name] = $view_display->display_title;
      }
    }

    return $option_list;
  }

  /**
   * Alter items before indexing.
   *
   * When the view returns 0 results, the item will be excluded from the index.
   *
   * @param array $items
   *   An array of items to be altered, keyed by item IDs.
   *
   * @throws \Exception
   */
  public function alterItems(array &$items) {
    if (empty($this->options['view'])) {
      throw new Exception('Search api views filter is not configured correctly. Please select the view or disable the filter');
    }

    foreach ($items as $index => $item) {
      if ($this->viewHasNoResultForIdentifier($item->{$this->getEntityKeyToArgument()})) {
        unset($items[$index]);
      }
    }
  }

  /**
   * Checks if the view result is empty.
   *
   * @param string $identifier
   *   The identifier of the entity.
   *
   * @return bool
   *   TRUE if no result, FALSE otherwise.
   */
  private function viewHasNoResultForIdentifier($identifier) {
    $view = $this->getViewForFilter();
    $view->set_arguments([$identifier]);
    $view->execute();

    return empty($view->result);
  }

  /**
   * Gets the view for the current filter.
   *
   * @return \view
   *   The view.
   */
  private function getViewForFilter() {
    if (NULL === $this->view) {
      $view_info = explode('::', $this->options['view']);
      $this->view = views_get_view($view_info[0]);
      $this->view->set_display($view_info[1]);
    }

    return $this->view;
  }

  /**
   * Gets the index to pass on to the view.
   *
   * @return string
   *   The index.
   *
   * @throws \Exception
   *   When not implemented.
   */
  private function getEntityKeyToArgument() {
    if ($this->index->getEntityType() === 'node') {
      return 'nid';
    }

    throw new Exception('Search api views filter is not yet implemented for this entity type');
  }

}
