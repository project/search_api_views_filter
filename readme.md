# Search api views filter

This search api filter allows you to exclude items from being indexed if a view returns
no results.

Use cases could be:
- Advanced filtering for product properties
- Conditional filtering based on dates

However, this module should be used with caution as it will execute the view for every
item that search api is going to index.

## Setup

Install and enable the module as any other module you would install.

On the search index settings page enable the `Filter by the outcome of a view` option.

Select the view display that you want to use for filtering. The first and currently only
argument that will be passed to the view is the node id. (Request other entity types to
be implemented.).

## Tips and guidelines

Keep the view simple, an heavy view could time out.

When indexing all items, keep the items per batch low, this to avoid php from timing out.

If you have can simply filter your view, then use that as it is more efficient that this.
